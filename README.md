# drupal-starter-repo

A repository template for a Drupal project.

Intended as a jumpstart for any new Drupal project with a well defined layout and workflow based on the installation profile/distribution approach, which uses drush make files to maintain dependencies and perform builds.

# Repo Structure

* `docroot` - project document root
* `drush` - custom drush commands, aliases, etc.
* `patches` - any required patches
* `profiles` - Drupal `profiles` folder, symlinked into `docroot/profiles`
* `sites` - Drupal `sites` folder, symlinked into `docroot/sites`
* `tests` - automated tests

# Getting Started 

1. Clone the repo
```
git clone --origin drupal-starter-repo https://github.com/blinkreaction/drupal-starter-repo.git myproject
cd myproject
git remote rm drupal-starter-repo
```

2. Add a default installation profile (TODO: decide if this is needed)
```
git clone https://github.com/blinkreaction/drupal-starter-distro.git profiles/starter
git add profiles/starter/
```
Note: `git add profiles/starter/` <-- trailing slash here is important (http://goo.gl/TPFqtx)

3. Build the docroot (from the installation profile)
```
cd docroot
drush make ../profiles/starter/build-starter.make ./
cd ..
```
OR
```
cd docroot
drush make http://goo.gl/7E9peu ./
cd ..
```

4. Commit and push everything to its new home
```
git add .
git commit -m "initial build"
git remote add origin <myproject remote repo URL>
git push origin master
```
Note: replace < myproject remote repo URL > with the actual remote repo URL

